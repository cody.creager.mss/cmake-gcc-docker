FROM gcc:10

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y -qq vim cmake bsdmainutils
